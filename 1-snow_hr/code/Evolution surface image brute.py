# -*- coding: utf-8 -*-
import os
from PIL import Image
import matplotlib.pyplot as plt 
import numpy as np 
import sys


liste=[] # liste des surfaces de neige / axe vertical
dates = [] # liste des dates auxquelles ont été prises les images / axe horizontal
photos = []
m=0
print("\nEntrez la valeur de l'échelle (longueur en mètre d'un pixel)")
scale = int(input()) # l'utilisateur entre la taille du côté d'un pixel, pour toutes les images
liste_pix_blancs = []
liste_pix_neige = []
pcneige = []

# Choix du dossier à analyser
print("\nQuel dossier voulez-vous analyser ?")
dossier = input() # Les photos doivent être au format aammjj%%% avec %%% le % de nuages
os.chdir(str(dossier))

for photo in os.listdir(): # Selection des photos dans le dossier
    if photo.endswith('.jpg') and len(photo) == 13: # On vérifie le bon format des images et le bon nombre de caractère 
        photos.append(photo) # Si elles sont du bon format et bien nommées, on les met dans la liste
if len(photos) == 0:
    print ( "\n Erreur : PAS DE PHOTO AU BON FORMAT")
    sys.exit()
    
    
print("\nComment voulez-vous nommer le fichier avec les données ?") # Choix du nom du fichier resultat
nom = input()
if nom+".txt" in os.listdir() :
    while nom+".txt" in os.listdir(): # Boucle pour le choix du nom
        print ("\nCe fichier existe déjà, voulez-vous l'écraser?\n\nOui : 1  Non : 0")
        m = int(input())
        if m == 1:
            os.remove(str(nom)+".txt")
        if m == 0:
            print("\nComment voulez-vous nommer le fichier avec les données ?")
            nom = input()
        
    
for i in range(len(photos)): # On trie le dossier par ordre croissant (pour le graphe a la fin)
       mini = i # On veut mettre le minimum au rang i
       for j in range(i+1, len(photos)): # On cherche le rang du minimum 
           if photos[mini] > photos[j]:
               mini = j
       va = photos[i] # On stocke la valeur du rang dans une variable auxiliaire pour ne pas l'ecraser
       photos[i] = photos[mini]
       photos[mini] = va
if len(photos) != 0:    
       print("\nLes photos analyses sont :",photos,"\n")




for photo in photos : # On attaque le traitement de tout le dossier
    print ("Analyse de",photo,"...\n")
    img = Image.open(photo) # img est l'image qu'on peut utiliser ici
    
# axe temporel
    date = 100000*int(photo[0])+10000*int(photo[1])+1000*int(photo[2])+100*int(photo[3])+10*int(photo[4])+int(photo[5])
    # on transforme la chaine de caractères en entier avec int
    dates.append(date) # on ajoute cette date à la liste de dates

# pixels
    taillex = img.size[0] # nombre de pixels sur une ligne (horizontal)
    tailley = img.size[1] # nbr de pixels sur une colonne (vertical)

# initialisations
    L = [] # liste des coordonnées des pixels blancs
    r,v,b = 0,0,0 # valeurs de Rouge, Vert, Bleu
    nb_pix_blancs = 0 # nombre de pixels blancs

# boucle : balayage de tous les pixels
    limite = 235 # définir la valeur des composantes à partir de laquelle le pixel est considéré blanc
    # avec la valeur 235, le gris clair n'est pas compté (essai effectué) : valeur cohérente
    for i in range(taillex): # on balaie l'horizontale
        for j in range (tailley): # on balaie ensuite la verticale : on procède par colonnes
            r,v,b=img.getpixel((i,j)) # r,v,b prennent les valeurs des 3 composantes du pixel (i,j)
            if r > limite and v > limite and b > limite: # si les 3 composantes sont claires
            # ces conditions fonctionnent : nb_pix_blancs varie de manière cohérente si on modifie la limite
                nb_pix_blancs += 1 # il y a un pixel blanc en plus
                L.append([i,j]) # et le couple de coordonnées de ce pixel est enregistré dans la liste
                
    liste_pix_blancs.append(nb_pix_blancs)
    
    
    ### 
    pcneige.append(int(100*nb_pix_blancs/(taillex*tailley)))
    ###
    
    
# Prise en compte des nuages, niveau 1
for photo in photos: # photos.index(photo) est le numéro de la photo qui est entrain d'être traitée
    pcnuages = 100*int(photo[6])+10*int(photo[7])+int(photo[8])
    nb_pix_nuages = int(taillex*tailley*pcnuages/100)
    # On différentie le traitement selon le pourcentage de nuages
    # Soit on retranche au nombre de pixels blancs le nombre de pixels correspondant au pourcentage de nuages
    # Soit on fait la moyenne du nombre de pixels de neige de la photo d'avant et de celle d'après
    if pcnuages < 15 or photos.index(photo) == 0 or photos.index(photo) == len(photos):
        nb_pix_neige = liste_pix_blancs[photos.index(photo)] - nb_pix_nuages
    else :
        nb_pix_neige = int((liste_pix_blancs[photos.index(photo)-1]+liste_pix_blancs[photos.index(photo)+1])/2)
    liste_pix_neige.append(nb_pix_neige)
          
    
    
# écriture des résultats dans un fichier texte créé dans le même dossier que les images

for photo in photos:
    fichier = open(str(nom)+".txt","a") # on ouvre le fichier texte que'on a préalablement nommé pour le compléter
    
    fichier.write("\n") # saute une ligne
    fichier.write(str(photo)) # on écrit le nom de l'image
    fichier.write("\nNombre de pixels de neige = ")
    fichier.write(str(liste_pix_neige[photos.index(photo)])) # on doit écrire une chaine de caractères : on transforme
    fichier.write("\nSurface totale de neige = ")
    fichier.write(str(liste_pix_neige[photos.index(photo)]*(scale**2)))
    fichier.write(" m²")
    fichier.write("\n____________") # délimitations entre les calculs
    fichier.close() # on ferme le fichier texte
    # affichages directement dans le terminal
    print("//",photo,"//\n\nNombre de pixels de neige = ",liste_pix_neige[photos.index(photo)])
    print("La surface totale de neige est de ",liste_pix_neige[photos.index(photo)]*(scale**2)," m²\n")
    

    
    
# ajout de la surface neigeuse dans la liste
    liste.append(int(liste_pix_neige[photos.index(photo)]*(scale**2)))
    # on ajoute cette valeur au même indice dans la liste que la date indiquée plus haut


# tracé une fois que toutes les images voulues ont été calculées
plt.figure()
plt.plot(dates,liste) # surface de neige en fonction de la date
plt.title("Evolution de la surface de neige au cours du temps")
plt.xlabel("Date en aa/mm/jj")
plt.ylabel("Surface en m²")
plt.show()
