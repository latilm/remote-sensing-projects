function [ HS,MS ] = l_source_S2_ms_hs(row_debut,row_fin,col_debut,col_fin)
%Chargement des donn�es Sentinel avec d�coupage sur la zone pr�d�finie et
%passage en r�flectance
%Bien changer les dates si une autre image est utilis�
% En sortie: HS : matrice des tuiles nativement � 20m
% En sortie: MS : matrice des tuiles nativement � 10m
addpath('../data/Sentinel-2/') % path to folder containing the S-2 bands

B2=double(imread('SENTINEL2A_20160922-103357-529_L2A_T31TGL_D_V1-0_SRE_B2.tif')); %10m 
B3=double(imread('SENTINEL2A_20160922-103357-529_L2A_T31TGL_D_V1-0_SRE_B3.tif')); %10m 
B4=double(imread('SENTINEL2A_20160922-103357-529_L2A_T31TGL_D_V1-0_SRE_B4.tif')); %10m 
B5=double(imread('SENTINEL2A_20160922-103357-529_L2A_T31TGL_D_V1-0_SRE_B5.tif')); %20m 
B6=double(imread('SENTINEL2A_20160922-103357-529_L2A_T31TGL_D_V1-0_SRE_B6.tif')); %20m 
B7=double(imread('SENTINEL2A_20160922-103357-529_L2A_T31TGL_D_V1-0_SRE_B7.tif')); %20m 
B8=double(imread('SENTINEL2A_20160922-103357-529_L2A_T31TGL_D_V1-0_SRE_B8.tif')); %10m 
B8A=double(imread('SENTINEL2A_20160922-103357-529_L2A_T31TGL_D_V1-0_SRE_B8A.tif')); %20m 
B11=double(imread('SENTINEL2A_20160922-103357-529_L2A_T31TGL_D_V1-0_SRE_B11.tif')); %20m 
B12=double(imread('SENTINEL2A_20160922-103357-529_L2A_T31TGL_D_V1-0_SRE_B12.tif')); %20m 





HS(:,:,1)=B5(row_debut:row_fin,col_debut:col_fin)./10000;
HS(:,:,2)=B6(row_debut:row_fin,col_debut:col_fin)./10000;
HS(:,:,3)=B7(row_debut:row_fin,col_debut:col_fin)./10000;
HS(:,:,4)=B8A(row_debut:row_fin,col_debut:col_fin)./10000;
HS(:,:,5)=B11(row_debut:row_fin,col_debut:col_fin)./10000;
HS(:,:,6)=B12(row_debut:row_fin,col_debut:col_fin)./10000;

MS(:,:,1)=B2(row_debut*2:row_fin*2+1,col_debut*2:col_fin*2+1)./10000;
MS(:,:,2)=B3(row_debut*2:row_fin*2+1,col_debut*2:col_fin*2+1)./10000;
MS(:,:,3)=B4(row_debut*2:row_fin*2+1,col_debut*2:col_fin*2+1)./10000;
MS(:,:,4)=B8(row_debut*2:row_fin*2+1,col_debut*2:col_fin*2+1)./10000;

% Uncomment the lines below if you want to force pixels to have values in the interval [0,1]. 
% Pixels with with reflectance values above 1 will be saturated to 1.
HS(HS>1) = 1;
MS(MS>1) = 1;

end

