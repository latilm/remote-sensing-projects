def rgb_to_gray(img):
        grayImage = np.zeros(img.shape)
        R = np.array(img[:, :, 0])
        G = np.array(img[:, :, 1])
        B = np.array(img[:, :, 2])

        R = (R *.299)
        G = (G *.587)
        B = (B *.114)

        Avg = (R+G+B)
        grayImage = img

        for i in range(3):
           grayImage[:,:,i] = Avg

        return grayImage

image = mpimg.imread(" #image founder path, the image must be in png ")
grayImage = rgb_to_gray(image)

a = np.shape(grayImage)
NB_img = np.zeros(grayImage.shape)
for k in range (3):
    for i in range (grayImage.shape[0]):
        for j in range (grayImage.shape[1]):
            if grayImage[i,j,k] < 0.23:
                NB_img[i,j,k] = 255
            else:
                NB_img[i,j,k] = 0

im2=ndimage.filters.laplace(NB_img)
plt.imshow(im2, cmap='gray')
plt.show()