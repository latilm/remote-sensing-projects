**2021 - README groupe 4 - Mathieu Chorel, Marie Latil, Martin Mercie, Mohamed Salem Abeid**


**"Lecture de la carte brut"** is a program to plot a map on a day. We can observe a colored zone corresponding to the zone scanned by the satellite. The scale indicates the NO2 content in the atmosphere.

**"Map seul"** allows us to display what we see on the raw map in a more precise zone. Be careful not to pick up clouds that can interfere with the data received.

**"Traitement 2"** will use multiple selected images to average the value for each pixel.

**"Carte 2"** will display the result of this program in the form of a map showing on one side a map representing the concentration of NO2 in the atmosphere, but in a more reliable way than the "map seul" program because the latter contains an average over a range of values. The second map is the number of pixels counted on the image. This is directly related to the qa_value. Indeed, it will tell us for each pixel, the number of pixels which was taken into account out of the total number of images.

**"Sélection automatique images tropomi"** is made in order to automatically select images directly on the Tropomi site using previously selected criteria. But it does not work.
